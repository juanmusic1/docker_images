# Escogemos como base una imagen de python 3.5 basado en alpine linux

FROM python:3.5.10-alpine3.12@sha256:04d09d0e3c0b4567d5dfa18e9341f28e4f1e2a293687c2b29f2f506e3a3b5fcf

# Agregamos una carpeta y su contenido a la raiz

ADD includes/* /

# Actualizamos la imagen y creamos el usuario test

RUN apk upgrade --update && \
     adduser -D -s /bin/sh -h /home/test test && \
     chown test: /home/test/ -R

# Configuramos el punto de entrada de nuestro hola mundo que lo ejecute el usuario test

ENTRYPOINT ["su","test", "-c", "exec python /hola_mundo.py"]
